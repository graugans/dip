/**
 * Copyright (c) 2013, Krzysztof Kli¿ <krzysztof.klis@gmail.com>
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice, this
 *    list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE FOR
 * ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#include "resolver.hpp"
#include <iostream>
#include <string>
#include <iomanip>
#include <ctime>
#include <chrono>


class ILogger 
{
public:
    virtual void log(const std::string& message) = 0;
};

class StdOutLogger : public ILogger 
{
public:
    	void log(const std::string& message) 
	{
		std::chrono::system_clock::time_point now = std::chrono::system_clock::now();
		auto duration = now.time_since_epoch();
		auto millis = std::chrono::duration_cast<std::chrono::milliseconds>(duration).count();
       		std::cout << "[LOG] <" 
			  << millis << "> " 
                          << message 
                          << std::endl;
    	}
};

class Foo {
  private:
    ILogger *mp_logger;
  public:
    Foo(ILogger *l) {
        mp_logger = l;
    }
    void doSomethig() {
        /* does something */
        mp_logger->log("done");
    }
};


int main()
{
	/* Bootstrap code */
	dij::Resolver::insert<ILogger>(new StdOutLogger());
	Foo *foo = new Foo(dij::Resolver::resolve<ILogger>());
	foo->doSomethig();

	return 0;
}
