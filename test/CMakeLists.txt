enable_testing()

message(STATUS "Building Unit Tests ${UNITTEST}")
find_package(CppUTest REQUIRED)


include_directories(
  ${CppUTest_INCLUDE_DIR}
  ${CppUTest_Ext_INCLUDE_DIR}
)

file(GLOB DI_TEST_SOURCES *.cpp)
add_executable(di-tests
	${DI_TEST_SOURCES}
)

target_link_libraries( di-tests
	${CppUTest_LIBRARY}
)

add_test(NAME di-tests COMMAND di-tests)


